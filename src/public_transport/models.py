from django.db import models


class Line(models.Model):
    name = models.CharField(max_length=255)
    product = models.CharField(max_length=255, null=True)

    def __str__(self):
        return f'{self.name} {self.product}'


class Station(models.Model):
    name = models.CharField(max_length=255)
    lat = models.FloatField()
    lon = models.FloatField()
    place = models.CharField(max_length=255)
    lines = models.ManyToManyField(Line, related_name='stations')

    def __str__(self):
        return f'{self.id} {self.name}'
