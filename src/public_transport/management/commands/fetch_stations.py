from django.core.management.base import BaseCommand
from ...utils.mvg import get_stations_at
from math import ceil
from ...models import Line, Station


class Command (BaseCommand):

    top_left_coords = (48.35, 11.35)
    bottom_right_coords = (48.0,  11.9)

    def handle(self, *args, **kwargs):
        step_length = 0.005
        lat_steps = ceil(
            abs(self.top_left_coords[0]-self.bottom_right_coords[0])/step_length)
        lon_steps = ceil(
            abs(self.top_left_coords[1]-self.bottom_right_coords[1])/step_length)

        for a in range(0, lat_steps):
            for b in range(0, lon_steps):
                lat = self.bottom_right_coords[0]+step_length*a
                lon = self.top_left_coords[1]+step_length*b
                data = get_stations_at(lat, lon)
                print('%d %d' % (a, b))
                for d in data:
                    lines = [Line.objects.get_or_create(
                        **l)[0] for l in d['lines']]
                    d.pop('lines')
                    station = Station(**d)
                    station.save()
                    station.lines.set(lines)
                    print(station)

    def get_max_coord_diffs(self):
        stations = get_stations_at(self.base_lat, self.base_lon)

        max_lat_diff = max(abs(x['lat']-self.base_lat) for x in stations)
        max_lon_diff = max(abs(x['lon']-self.base_lon) for x in stations)

        return (max_lat_diff, max_lon_diff)
