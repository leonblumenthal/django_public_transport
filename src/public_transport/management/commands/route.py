from django.core.management.base import BaseCommand
from ...models import Line, Station
from ...utils.route import explore


class Command (BaseCommand):

    def handle(self, *args, **kwargs):
        start = Station.objects.get(id=900)
        target = Station.objects.get(id=460)
        route = explore(start, target)
        print(route)
