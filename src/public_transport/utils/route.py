from .mvg import get_stations
from ..models import Station, Line


def explore(start, target):
    visited = []
    found = False
    routes = [[start]]
    while not found:
        new_routes = []
        for route in routes:
            for line in route[-1].lines.all():
                for station in line.stations.all():
                    if station not in route[0:-1]:
                        r = route.copy()
                        r.append(station)
                        if station == target:
                            found = True
                            return r
                        new_routes.append(r)
        routes = new_routes
