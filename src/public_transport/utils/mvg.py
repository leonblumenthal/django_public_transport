import requests
import json
import datetime


api_key = "5af1beca494712ed38d313714d4caff6"
query_url = "https://www.mvg.de/fahrinfo/api/location/query?q=%s"
departure_url = "https://www.mvg.de/fahrinfo/api/departure/%s?footway=0"
nearby_url = "https://www.mvg.de/fahrinfo/api/location/nearby?latitude=%s&longitude=%s"
interruptions_url = "https://www.mvg.de/.rest/betriebsaenderungen/api/interruptions"


def _get(url):
    resp = requests.get(url, headers={'X-MVG-Authorization-Key': api_key})
    return resp.json()


def get_departures(station_id):
    departure_data = _get(departure_url % str(station_id))['departures']

    return [
        {
            'time': datetime.datetime.fromtimestamp(data['departureTime']/1e3),
            'destination': data['destination'],
            'label': data['label'],
            'product': data['product'].lower(),
            'color': data['lineBackgroundColor'].split(',')[0],
            'sev': data['sev'],
            'live': data['live']
        }
        for data in departure_data
    ]


def get_station_id(station_name):
    return get_stations(station_name)[0]['id']


def _filter_stations(data):
    for d in data:
        if len(d) == 0:
            return []
        line_data = d['lines']
        lines = []
        for product in line_data:
            for line in line_data[product]:
                lines.append({'name': line, 'product': product})
        d['lines'] = lines
    return [
        {
            'id': d['id'],
            'name': d['name'],
            'place':d['place'],
            'lines': d['lines'],
            'lat': d['latitude'],
            'lon': d['longitude'],
        }
        for d in data if d['type'] == 'station' or d['type'] == 'nearbystation'
    ]


def get_stations(search):
    location_data = _get(query_url % search)['locations']
    return _filter_stations(location_data)


def get_stations_at(lat, lon):
    location_data = _get(nearby_url % (lat, lon))['locations']
    return _filter_stations(location_data)
