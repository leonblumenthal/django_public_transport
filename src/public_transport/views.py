from .models import Station
from .serializers import StationSerializer
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status

from .utils.mvg import get_stations
from .utils.route import explore


@api_view(['GET'])
def route(request):
    try:
        start_name = request.GET['start']
        target_name = request.GET['target']
        start_data = get_stations(start_name)[0]
        target_data = get_stations(target_name)[0]
        start = Station.objects.get(id=start_data['id'])
        target = Station.objects.get(id=target_data['id'])
        r = explore(start, target)
        serializer = StationSerializer(r, many=True)
        return Response(serializer.data)
    except KeyError:
        return Response(status=status.HTTP_400_BAD_REQUEST)



